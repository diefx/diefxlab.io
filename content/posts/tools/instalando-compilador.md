---
title: "Instalando Compilador"
date: 2021-04-27T16:15:18-05:00
draft: false
---

Empezaremos a instalar las herramientas que necesitaremos para trabajar y la primera de ellas sera nuestro compilador [**GCC**](https://gcc.gnu.org/) y su debugger [**GDB**](https://www.gnu.org/software/gdb/), por que no debe faltar un buen compilador para crear programas en C para nuestra computadora.

```bash
$ sudo pacman -S gcc gdb
```

Comprobamos la instalación preguntando la versión del compilador

```bash
$ gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/lto-wrapper
Target: x86_64-pc-linux-gnu
Configured with: /build/gcc/src/gcc/configure --prefix=/usr --libdir=/usr/lib --libexecdir=/usr/lib --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=https://bugs.archlinux.org/ --enable-languages=c,c++,ada,fortran,go,lto,objc,obj-c++,d --with-isl --with-linker-hash-style=gnu --with-system-zlib --enable-__cxa_atexit --enable-cet=auto --enable-checking=release --enable-clocale=gnu --enable-default-pie --enable-default-ssp --enable-gnu-indirect-function --enable-gnu-unique-object --enable-install-libiberty --enable-linker-build-id --enable-lto --enable-multilib --enable-plugin --enable-shared --enable-threads=posix --disable-libssp --disable-libstdcxx-pch --disable-libunwind-exceptions --disable-werror gdc_include_dir=/usr/include/dlang/gdc
Thread model: posix
Supported LTO compression algorithms: zlib zstd
gcc version 10.2.0 (GCC) 
```

Ok, enseguida instalaremos [**make**](https://www.gnu.org/software/make/) el cual nos ayudara a que nuestros procesos de compilación sean mas sencillos y no tengamos que escribir tantas lineas en la terminal.

```bash
$ sudo pacman -S make
```

De nueva cuenta preguntamos versión

```bash
$ make -v
GNU Make 4.3
Built for x86_64-pc-linux-gnu
Copyright (C) 1988-2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```

Y por ultimo instalaremos el compilador para microcontroladores [**Cortex-M**](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm). El compilador es basado en **GCC** y ademas es totalmente libre y lo mejor es que no presenta limitación alguna =). En la terminal escribe:

```bash
$ sudo pacman -S arm-none-eabi-gcc arm-none-eabi-gdb arm-none-eabi-newlib 
```

Si observas estamos instalando tres paquetes, el primero es el propio compilador, mientras que el segundo es una versión reducida de la librería estándar de C para microcontroladores llamada [**Newlib**](https://sourceware.org/newlib/), y el tercero es el debugger que nos permitirá depurar nuestros programas.

Después de instalar comprobamos que todo este bien escribiendo en la terminal:

```bash
$ arm-none-eabi-gcc -v
Using built-in specs.
COLLECT_GCC=arm-none-eabi-gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/arm-none-eabi/10.3.0/lto-wrapper
Target: arm-none-eabi
Configured with: /build/arm-none-eabi-gcc/src/gcc-10.3.0/configure --target=arm-none-eabi --prefix=/usr --with-sysroot=/usr/arm-none-eabi --with-native-system-header-dir=/include --libexecdir=/usr/lib --enable-languages=c,c++ --enable-plugins --disable-decimal-float --disable-libffi --disable-libgomp --disable-libmudflap --disable-libquadmath --disable-libssp --disable-libstdcxx-pch --disable-nls --disable-shared --disable-threads --disable-tls --with-gnu-as --with-gnu-ld --with-system-zlib --with-newlib --with-headers=/usr/arm-none-eabi/include --with-python-dir=share/gcc-arm-none-eabi --with-gmp --with-mpfr --with-mpc --with-isl --with-libelf --enable-gnu-indirect-function --with-host-libstdcxx='-static-libgcc -Wl,-Bstatic,-lstdc++,-Bdynamic -lm' --with-pkgversion='Arch Repository' --with-bugurl=https://bugs.archlinux.org/ --with-multilib-list=rmprofile
Thread model: single
Supported LTO compression algorithms: zlib zstd
gcc version 10.3.0 (Arch Repository) 
```

Con el compilador instalado ya podemos empezar a escribir código en nuestro editor de código favorito (el favorito sera el de tu elección) ojo una cosa es editor y otro es un ambiente de desarrollo integrado, pero aquí no usaremos de los segundos. Editores hay muchos y muy variados como Atom, Vim, Emacs, **VS Code**, etc ..
