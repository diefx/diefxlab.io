---
title: "Instalalando Openocd"
date: 2021-04-27T17:04:59-05:00
draft: false
---

Ya con el compilador instalado, deberemos instalar un programa que nos permita comunicarnos con nuestro hardware y programarlo. Para esta tarea usaremos [**OpenOCD**](http://openocd.org/), el cual es un pequeño servidor que se conectará con nuestro programador/debugger y nos permitirá pasar a nuestro micro el programa ya compilado.

Su instalación deberá realizarse sin mayor problema, en la terminal solo escribe:

```bash
$ sudo pacman -S openocd
```

Verificamos si la instalación se ejecuto adecuadamente

```bash
$ openocd -v
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
```

Con la instalación de **OpenOCD** se nos genero en nuestra computadora una carpeta muy importante `/usr/local/share/openocd/scripts`, aquí es donde quedaron instalados los scripts que nos servirán para identificar el micro que queremos programar ademas de contener la información necesaria para decirle a **OpenOCD** como programarlo, estos scripts están agrupados en tres niveles:

- **target**.- scripts con instrucciones especificas para microcontroladores/procesadores
- **interface**.- scripts con la información del debugger/programador a usar
- **board**.- scripts con instrucciones que combinan interface y target. Y pertenecen a tarjetas especificas que ya están en el mercado

Bueno basta de Charla, hay que probar si realmente funciona. Para ello usaremos nuestra tarjeta **Nucleo-F072RB** de la marca ST, el cual por cierto posee un micro con **CPU Cortex-M0**, conectala al puerto USB ( _la PC la identificara como un mass storage, no te apures es normal_ ) y en la terminal ejecuta OpenOCD indicando el programador ( _stlink_ ) y el micro ( _stm32f0_ ) de la siguiente manera.

```bash
$ sudo openocd -f interface/stlink-v2-1.cfg -f target/stm32f0x.cfg
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : auto-selecting first available session transport "hla_swd". To override use 'transport select <transport>'.
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 1000 kHz
adapter_nsrst_delay: 100
none separate
Info : Unable to match requested speed 1000 kHz, using 950 kHz
Info : Unable to match requested speed 1000 kHz, using 950 kHz
Info : clock speed 950 kHz
Info : STLINK v2 JTAG v23 API v2 SWIM v6 VID 0x0483 PID 0x374B
Info : using stlink api v2
Info : Target voltage: 3.143795
Info : stm32f0x.cpu: hardware has 4 breakpoints, 2 watchpoints
```

El anterior mensaje aparecerá si no surgió un error en la conexión con tu tarjeta, **OpenOCD** ya esta conectado y esperando instrucciones, pero por ahora solo desconectate pulsando `Ctrl+C`.

No es recomendable ejecutar openocd con **sudo**, tampoco es algo cómodo. Asi que vamos quitando ese pequeño inconveniente, para lograrlo habrá que crear algunos permisos de ejecución en linux.

Verificamos si el grupo `plugdev` existe

```bash
$ grep plugdev /etc/group
```

si lo anterior no regresa nada, entonces no existe y hay que crearlo y también agregarle tu usuario

```bash
$ sudo groupadd plugdev
$ sudo usermod -a -G plugdev <username>  #reemplaza <username> con tu nombre de usuario
```

Copiamos cierto archivo con las reglas de acceso de openocd y las activamos. **NOTA** _es probablemente que el nombre del archivo sea diferente, para saberlo solo da un `ls /usr/share/openocd/contrib` y revisa que archivo te aparece_.

```bash
$ sudo cp /usr/share/openocd/contrib/60-openocd.rules /etc/udev/rules.d/
$ sudo udevadm control --reload-rules
$ sudo udevadm trigger
```

Listo, ya no tienes porque invocar **openocd** mediante **sudo**. Ahora tienes todo lo que necesitas para programar micros Cortex-M en tu Arch linux. Diviértete. 
