---
title: "Bienvenidos"
date: 2021-04-27T14:51:53-05:00
draft: false
---



Hola a todos y bienvenidos a mi libro de apuntes personales que espero les sirva de utilidad. Esta pagina nació de la necesidad de retener lo que eh aprendido en el pasado ya que no poseo buena memoria y cierto amigo me sugirió crear un blog. Todo lo que que aquí encontraras esta relacionado a la programación de microcontroladores. Antes de empezar a leer los posts me gustaría aclarar algunas cosas:

- En el 99% de las ocasiones se usara la linea de comandos por lo que te sugiero que como mínimo aprendas a usar los comandos como `ls, mkdir, touch, cp, mv, rm`. puedes encontrar información sobre ellos por todo el internet o en este muy buen [**libro**](http://linuxcommand.org/tlcl.php).

- El sistema operativo que estaré usando para trabajar es **Linux** en su distribución Arch, porque no windows?, pues porque sencillamente es mas fácil trabajar con linux en especial cuando ya estas en un nivel avanzado.

- Para los novatos les recomiendo instalar [**Manjaro**](https://manjaro.org/) en cualquiera de sus sabores ( KDE, Gnome o XFCE ) no habrá mucha diferencia ya que haremos uso y abuso de la terminal.

- Si estas usando alguna otra distribución de linux que no sea basada en Arc o Windows o tal vez Mac, no te preocupes porque te mostrare como instalar las herramientas para cada uno de ellos y de ahí en adelante todo lo que hagamos sera igual en cualquier sistema operativo.

- El hardware que usare como referencia es la tarjeta **Nucleo-F072RB** la cual trae un micro con un cpu **Cortex-M0**. pero la gran mayoría de los conceptos aplicaran a cualquier otro micro de cualquier fabricante (siempre y cuando tengan micros basados en Cortex-M), puedes usar las tarjetas Freedom de NXP, TI launchpad, etc..

- El editor que yo uso es **visual estudio code**, tu puedes usar el que gustes, pero en los posts observaras que es el que yo utilizo cuando indico que hay que escribir algo en algún archivo.

- Otra de mis sugerencias es que aprendas a usar **git** ya que es el sistema de versionado y distribución de proyectos mas popular que existe y para algunas cosas haremos uso de el.

- Tengo pésima ortografía, asi que no empiecen


